package com.hirsz.currencyexchange

import com.hirsz.currencyexchange.account.model.Account
import com.hirsz.currencyexchange.account.model.AccountCurrency
import com.hirsz.currencyexchange.account.model.Pesel
import com.hirsz.currencyexchange.account.usecases.CreateAccountUseCase
import com.hirsz.currencyexchange.services.currencyExchangeService.model.ExchangeRateRequest
import com.hirsz.currencyexchange.services.currencyExchangeService.model.ExchangeRateResponse
import java.math.BigDecimal

fun account() = Account("John", "Smith", Pesel("99010100000"))

fun createAccountRequest() = CreateAccountUseCase.CreateRequest(
        firstName = "FirstName",
        lastName = "LastName",
        pesel = "99010100000"
)

fun exchangeRateResponse() = ExchangeRateResponse(
        AccountCurrency.PLN,
        AccountCurrency.USD,
        BigDecimal.ONE)

fun exchangeRateRequest() = ExchangeRateRequest(AccountCurrency.PLN, AccountCurrency.USD)