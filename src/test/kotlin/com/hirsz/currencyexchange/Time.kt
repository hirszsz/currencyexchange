package com.hirsz.currencyexchange

import java.time.Clock
import java.time.Instant
import java.time.ZoneId
import java.time.temporal.TemporalUnit

val FixedClock: Clock = Clock.fixed(Instant.parse("2022-07-23T12:00:00.000000Z"), ZoneId.of("UTC"))

class EditableClock(private var clock: Clock) : Clock() {

    override fun instant(): Instant =
        clock.instant()

    override fun withZone(zone: ZoneId): Clock = clock.withZone(zone)

    override fun getZone(): ZoneId = clock.zone

    fun advanceTimeBy(amount: Long, unit: TemporalUnit) {
        clock = fixed(clock.instant().plus(amount, unit), clock.zone)
    }
}