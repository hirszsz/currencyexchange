package com.hirsz.currencyexchange.account.respository

import arrow.core.left
import arrow.core.right
import com.hirsz.currencyexchange.account.repository.InMemoryAccountRepository
import com.hirsz.currencyexchange.account.model.Account
import com.hirsz.currencyexchange.account.model.AccountError
import com.hirsz.currencyexchange.account
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AccountRepositoryTest {

    private fun createRepository(initialData: List<Account> = emptyList()) = InMemoryAccountRepository(initialData)

    @Test
    fun shouldFindAccount() {
        val account = account()
        val repository = createRepository(listOf(account))

        val fetchResult = repository.find(account.getPeselNumber())

        assertEquals(account.right(), fetchResult)
    }

    @Test
    fun shouldInsertAccount() {
        val account = account()
        val repository = createRepository()

        val insertResult = repository.insert(account)

        val fetchResult = repository.find(account.getPeselNumber())
        assertEquals(account.right(), insertResult)
        assertEquals(account.right(), fetchResult)
    }

    @Test
    fun shouldNotInsertAccountWithDuplicatedPesel() {
        val account = account()
        val repository = createRepository(listOf(account))

        val insertResult = repository.insert(account)

        assertEquals(AccountError.AccountWithPeselAlreadyRegistered(account.getPeselNumber()).left(), insertResult)
    }

    @Test
    fun shouldUpdateAccount() {
        val initAccount = account()
        val repository = createRepository(listOf(initAccount))
        val updatedAccount = account().copy(
                firstName = "NewFirstName",
                lastName = "NewLastName"
        )

        val updateResult = repository.update(updatedAccount)

        val fetchResult = repository.find(updatedAccount.getPeselNumber())
        assertEquals(updatedAccount.right(), updateResult)
        assertEquals(updatedAccount.right(), fetchResult)
    }

    @Test
    fun shouldNotUpdateAccountIfNotExists() {
        val repository = createRepository()
        val account = account()

        val updateResult = repository.update(account)

        assertEquals(AccountError.AccountWithPeselNotRegistered(account.getPeselNumber()).left(), updateResult)
    }

}