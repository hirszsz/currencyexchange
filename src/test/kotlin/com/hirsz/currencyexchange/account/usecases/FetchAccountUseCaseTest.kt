package com.hirsz.currencyexchange.account.usecases

import arrow.core.right
import com.hirsz.currencyexchange.account.repository.InMemoryAccountRepository
import com.hirsz.currencyexchange.account.model.Account
import com.hirsz.currencyexchange.account
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FetchAccountUseCaseTest {

    private fun createUseCase(initialData: List<Account> = emptyList()) = FetchAccountUseCaseImplementation(InMemoryAccountRepository(initialData))

    @Test
    fun shouldCreateAccount() {
        val initAccount = account()
        val useCase = createUseCase(listOf(initAccount))

        val createResult = useCase.find(initAccount.getPeselNumber())

        assertEquals(initAccount.right(), createResult)
    }




}