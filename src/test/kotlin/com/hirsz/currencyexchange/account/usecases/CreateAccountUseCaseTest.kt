package com.hirsz.currencyexchange.account.usecases

import arrow.core.left
import arrow.core.right
import com.hirsz.currencyexchange.FixedClock
import com.hirsz.currencyexchange.account.repository.InMemoryAccountRepository
import com.hirsz.currencyexchange.account.model.Account
import com.hirsz.currencyexchange.account.model.AccountError
import com.hirsz.currencyexchange.account.model.Pesel
import com.hirsz.currencyexchange.account
import com.hirsz.currencyexchange.createAccountRequest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CreateAccountUseCaseTest {

    private fun createUseCase(initialData: List<Account> = emptyList()) =
            CreateAccountUseCaseImplementation(
                    repository = InMemoryAccountRepository(initialData),
                    18,
                    FixedClock
            )

    @Test
    fun shouldCreateAccount() {
        val useCase = createUseCase()
        val createAccountRequest = createAccountRequest()
        val expected = account().copy(
                firstName = createAccountRequest.firstName,
                lastName = createAccountRequest.lastName,
                pesel = Pesel(createAccountRequest.pesel)
        )

        val createResult = useCase.create(createAccountRequest)

        assertEquals(expected.right(), createResult)
    }

    @Test
    fun shouldNotAllowCreateAccountForUnderageUser() {
        val useCase = createUseCase()
        val createAccountRequest = createAccountRequest().copy(
                pesel = "04272400000"
        )

        val createResult = useCase.create(createAccountRequest)

        assertEquals(AccountError.RequiredAgeNotReached(18).left(), createResult)
    }

    @Test
    fun shouldNotAllowCreateTwoAccountWithTheSamePesel() {
        val useCase = createUseCase()
        val pesel = "99040100000"
        useCase.create(createAccountRequest().copy(
                pesel = pesel
        ))


        val createResult = useCase.create(createAccountRequest().copy(
                pesel = pesel,
                firstName = "OtherFirstName",
                lastName = "OtherLastName"
        ))

        assertEquals(AccountError.AccountWithPeselAlreadyRegistered(pesel).left(), createResult)
    }
}