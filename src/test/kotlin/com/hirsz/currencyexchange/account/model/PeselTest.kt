package com.hirsz.currencyexchange.account.model

import arrow.core.left
import arrow.core.right
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDate

class PeselTest {

    @Test
    fun shouldGetDayOfBrithFromPesel(){
        birthDayFromPeselEquals(
                "0182030000",
                LocalDate.of(1801, 2, 3))
        birthDayFromPeselEquals(
                "0203040000",
                LocalDate.of(1902, 3, 4))
        birthDayFromPeselEquals(
                "0425060000",
                LocalDate.of(2004, 5, 6))
    }

    @Test
    fun shouldReturnErrorWhenDateNotExists(){
        val peselNumber = "0100020000"
        Assertions.assertEquals(
                Pesel(peselNumber).getDayOfBrith(),
                AccountError.InvalidPeselFormat(peselNumber).left()
        )
    }

    @Test
    fun shouldReturnErrorForNonNumberFormat(){
        val peselNumber = "e001020000"
        Assertions.assertEquals(
                Pesel(peselNumber).getDayOfBrith(),
                AccountError.InvalidPeselFormat(peselNumber).left()
        )
    }

    @Test
    fun shouldReturnErrorForNotSuppoertedMouthRange(){
        val peselNumber = "0170020000"
        Assertions.assertEquals(
                Pesel(peselNumber).getDayOfBrith(),
                AccountError.InvalidPeselFormat(peselNumber).left()
        )
    }

    private fun birthDayFromPeselEquals(peselNumber: String, date: LocalDate) {
        Assertions.assertEquals(
                Pesel(peselNumber).getDayOfBrith(),
                date.right()
        )
    }
}