package com.hirsz.currencyexchange.service

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.hirsz.currencyexchange.account.model.AccountCurrency
import com.hirsz.currencyexchange.configuration.NBPApiProperties
import com.hirsz.currencyexchange.exchangeRateRequest
import com.hirsz.currencyexchange.exchangeRateResponse
import com.hirsz.currencyexchange.services.currencyExchangeService.nbp.NBPCurrencyExchangeRateService
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class NBPCurrencyExchangeRateServiceTest {

    private val service = NBPCurrencyExchangeRateService(NBPApiProperties(
            "http://$host:$port",
            4
    ))


    companion object {
        private val wireMockServer = WireMockServer(port)

        @BeforeAll
        @JvmStatic
        private fun startWireMock() {
            wireMockServer.start()
        }

        @AfterAll
        @JvmStatic
        private fun stopWireMock() {
            wireMockServer.stop()
        }
    }

    @Test
    fun shouldFetchUSDToPLNExchangeRate() {
        stubWiremock()

        val getResult = service.getExchangeRate(exchangeRateRequest().copy(
                currencyForSale = AccountCurrency.PLN,
                currencyToBuy = AccountCurrency.USD
        )).orNull()

        assertEquals(exchangeRateResponse().copy(
                currencyForSale = AccountCurrency.PLN,
                currencyToBuy = AccountCurrency.USD,
                exchangeRate = BigDecimal.valueOf(0.2171)
        ), getResult)
    }


    @Test
    fun shouldFetchPLNToUSDExchangeRate() {
        stubWiremock()

        val getResult = service.getExchangeRate(exchangeRateRequest().copy(
                currencyForSale = AccountCurrency.USD,
                currencyToBuy = AccountCurrency.PLN
        )).orNull()

        assertEquals(exchangeRateResponse().copy(
                currencyForSale = AccountCurrency.USD,
                currencyToBuy = AccountCurrency.PLN,
                exchangeRate = BigDecimal.valueOf(4.6062)
        ), getResult)
    }

    private fun stubWiremock() = stubFor(get(urlEqualTo("/api/exchangerates/rates/c/usd/")).willReturn(aResponse().withBody(NBPJsonResponse)))

}

private val NBPJsonResponse =
        "{\n" +
                "    \"table\": \"C\",\n" +
                "    \"currency\": \"dolar amerykański\",\n" +
                "    \"code\": \"USD\",\n" +
                "    \"rates\": [\n" +
                "        {\n" +
                "            \"no\": \"141/C/NBP/2022\",\n" +
                "            \"effectiveDate\": \"2022-07-22\",\n" +
                "            \"bid\": 4.6062,\n" +
                "            \"ask\": 4.6992\n" +
                "        }\n" +
                "    ]\n" +
                "}".trimIndent()

private const val host = "localhost"
private const val port = 8080