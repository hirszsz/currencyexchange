package com.hirsz.currencyexchange.web

import net.minidev.json.JSONObject
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvcResultMatchersDsl
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

class AccountIntegrationTest : IntegrationTest() {

    @Test
    fun `can create account`() {
        createAccount("90010100000").andExpect {
            status { isCreated() }
            assertAccountResponseBody("90010100000")
        }
    }

    @Test
    fun `can fetch account`() {
        val pesel = "90010100001"
        createAccount(pesel)

        mockMvc.get("/api/account/${pesel}").andExpect {
            status { isOk() }
            assertAccountResponseBody(pesel)
        }
    }


    private fun createAccount(pesel: String) = mockMvc.post("/api/account") {
        contentType = MediaType.APPLICATION_JSON
        content = createAccountRequest(pesel)
    }

    companion object {
        private fun createAccountRequest(pesel: String) = JSONObject()
                .appendField("firstName", "John")
                .appendField("lastName", "Smith")
                .appendField("pesel", pesel)
                .toString()

        private fun MockMvcResultMatchersDsl.assertAccountResponseBody(pesel: String) {
            jsonPath("firstName", Matchers.equalTo("John"))
            jsonPath("lastName", Matchers.equalTo("Smith"))
            jsonPath("pesel", Matchers.equalTo(pesel))
        }
    }
}