package com.hirsz.currencyexchange.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding


@ConstructorBinding
@ConfigurationProperties(prefix = "nbp")
data class NBPApiProperties(
        val apiUrl: String,
        val roundingScale: Int
)