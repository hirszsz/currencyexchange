package com.hirsz.currencyexchange.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "accounts")
data class AccountProperties (val minimumAccountAge: Int)