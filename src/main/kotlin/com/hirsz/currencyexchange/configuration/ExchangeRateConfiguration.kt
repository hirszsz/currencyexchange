package com.hirsz.currencyexchange.configuration

import com.hirsz.currencyexchange.services.currencyExchangeService.CurrencyRateExchangeService
import com.hirsz.currencyexchange.services.currencyExchangeService.HardcodedCurrencyExchangeRateService
import com.hirsz.currencyexchange.services.currencyExchangeService.nbp.NBPCurrencyExchangeRateService
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
@EnableConfigurationProperties(NBPApiProperties::class)
class ExchangeRateConfiguration {

    @Bean
    @Profile("test")
    fun exchangeRateService(): CurrencyRateExchangeService =
            HardcodedCurrencyExchangeRateService()

    @Bean
    @Profile("!test")
    fun exchangeRateService(nbpApiProperties: NBPApiProperties): CurrencyRateExchangeService =
            NBPCurrencyExchangeRateService(nbpApiProperties)

}