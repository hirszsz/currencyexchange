package com.hirsz.currencyexchange.configuration

import com.hirsz.currencyexchange.account.AccountFacade
import com.hirsz.currencyexchange.services.currencyExchangeService.CurrencyRateExchangeService
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Clock

@Configuration
@EnableConfigurationProperties(AccountProperties::class)
class AccountConfiguration {

    @Bean
    fun accountFacade(accountProperties: AccountProperties,
                      clock: Clock,
                      exchangeRateService: CurrencyRateExchangeService
    ): AccountFacade = AccountFacade.create(
                    accountProperties = accountProperties,
                    clock = clock,
                    exchangeService = exchangeRateService)
}