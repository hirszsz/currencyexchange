package com.hirsz.currencyexchange.account

import com.hirsz.currencyexchange.account.repository.InMemoryAccountRepository
import com.hirsz.currencyexchange.account.usecases.*
import com.hirsz.currencyexchange.configuration.AccountProperties
import com.hirsz.currencyexchange.services.currencyExchangeService.CurrencyRateExchangeService
import java.time.Clock

class AccountFacade(
        val createAccountUseCase: CreateAccountUseCase,
        val fetchAccountUseCase: FetchAccountUseCase,
        val exchangeUseCase: ExchangeUseCase
) :
        CreateAccountUseCase by createAccountUseCase,
        FetchAccountUseCase by fetchAccountUseCase,
        ExchangeUseCase by exchangeUseCase {

    companion object {
        fun create(accountProperties: AccountProperties,
                   clock: Clock,
                   exchangeService: CurrencyRateExchangeService
        ): AccountFacade {
            val repository = InMemoryAccountRepository()
            return AccountFacade(
                    createAccountUseCase = CreateAccountUseCaseImplementation(repository, accountProperties.minimumAccountAge, clock),
                    fetchAccountUseCase = FetchAccountUseCaseImplementation(repository),
                    exchangeUseCase = ExchangeUseCaseImplementation(exchangeService = exchangeService)
            )
        }
    }
}