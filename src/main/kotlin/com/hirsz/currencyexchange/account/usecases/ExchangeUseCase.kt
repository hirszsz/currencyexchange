package com.hirsz.currencyexchange.account.usecases

import com.hirsz.currencyexchange.services.currencyExchangeService.CurrencyRateExchangeService

interface ExchangeUseCase {
    fun exchange()
}

class ExchangeUseCaseImplementation(private val exchangeService: CurrencyRateExchangeService) : ExchangeUseCase {
    override fun exchange() {
        TODO("Not yet implemented")
    }

}