package com.hirsz.currencyexchange.account.usecases

import arrow.core.Either
import arrow.core.continuations.either.eager
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.hirsz.currencyexchange.CurrencyExchangeValidator
import com.hirsz.currencyexchange.account.model.Account
import com.hirsz.currencyexchange.account.model.AccountError
import com.hirsz.currencyexchange.account.model.Pesel
import com.hirsz.currencyexchange.account.repository.AccountRepository
import com.hirsz.currencyexchange.account.usecases.CreateAccountUseCase.CreateRequest
import java.time.Clock
import java.time.LocalDate
import java.time.Period
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Pattern

interface CreateAccountUseCase {

    fun create(request: CreateRequest): Either<AccountError, Account>

    data class CreateRequest(
            @get:NotEmpty
            val firstName: String,
            @get:NotEmpty
            val lastName: String,
            @get:Pattern(regexp = "^\\d{11}\$")
            val pesel: String
    ) {
        fun toAccount() = Account(
                firstName = firstName,
                lastName = lastName,
                pesel = Pesel(pesel)
        )

        companion object
    }
}

class CreateAccountUseCaseImplementation(private val repository: AccountRepository,
                                         private val minimumAccountAge: Int,
                                         private val clock: Clock) : CreateAccountUseCase {
    override fun create(request: CreateRequest): Either<AccountError, Account> = eager {
        checkRequestIsValid(request).bind()
        val account = request.toAccount()

        account.checkIfHasMinAge().bind()

        repository.insert(account).bind()
    }

    private fun checkRequestIsValid(request: CreateRequest): Either<AccountError, Unit> = eager {
        CurrencyExchangeValidator.validate(request)
                .mapLeft { AccountError.ValidationError(it) }.bind()

    }

    private fun Account.checkIfHasMinAge(): Either<AccountError, Unit> =
            this.pesel.getDayOfBrith()
                    .map { dayOfBrith -> Period.between(dayOfBrith, LocalDate.now(clock)) }
                    .flatMap { age ->
                        if (age.years < minimumAccountAge) {
                            AccountError.RequiredAgeNotReached(minimumAccountAge).left()
                        } else {
                            Unit.right()
                        }
                    }
}
