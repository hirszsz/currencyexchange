package com.hirsz.currencyexchange.account.usecases

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.rightIfNotNull
import com.hirsz.currencyexchange.account.model.Account
import com.hirsz.currencyexchange.account.model.AccountError
import com.hirsz.currencyexchange.account.repository.AccountRepository

interface FetchAccountUseCase {
    fun find(pesel: String): Either<AccountError, Account>
}

class FetchAccountUseCaseImplementation(private val repository: AccountRepository): FetchAccountUseCase {

    override fun find(pesel: String): Either<AccountError, Account> {
        return repository.find(pesel)
                .flatMap { account ->
                    account.rightIfNotNull { AccountError.AccountNotFound(pesel) }
                }
    }
}