package com.hirsz.currencyexchange.account.repository

import arrow.core.Either
import com.hirsz.currencyexchange.account.model.Account
import com.hirsz.currencyexchange.account.model.AccountError

interface AccountRepository {
    fun insert(account: Account): Either<AccountError, Account>
    fun update(account: Account): Either<AccountError, Account>
    fun find(pesel: String): Either<AccountError, Account?>
}