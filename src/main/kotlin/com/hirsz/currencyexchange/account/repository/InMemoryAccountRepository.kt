package com.hirsz.currencyexchange.account.repository

import arrow.core.Either
import arrow.core.right
import arrow.core.rightIfNotNull
import arrow.core.rightIfNull
import com.hirsz.currencyexchange.account.model.Account
import com.hirsz.currencyexchange.account.model.AccountError
import java.util.concurrent.ConcurrentHashMap

class InMemoryAccountRepository(initialData: List<Account> = emptyList()) : AccountRepository {
    private val storage: ConcurrentHashMap<String, Account> = ConcurrentHashMap(initialData.associateBy { account -> account.getPeselNumber() })

    override fun insert(account: Account): Either<AccountError, Account> {
        return storage.putIfAbsent(account.getPeselNumber(), account)
                .rightIfNull { AccountError.AccountWithPeselAlreadyRegistered(account.getPeselNumber()) }
                .map { account }
    }

    override fun update(account: Account): Either<AccountError, Account> {
        return storage.replace(account.getPeselNumber(), account)
                .rightIfNotNull { AccountError.AccountWithPeselNotRegistered(account.getPeselNumber()) }
                .map { account }
    }

    override fun find(pesel: String): Either<AccountError, Account?> = storage[pesel].right()

}