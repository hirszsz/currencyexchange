package com.hirsz.currencyexchange.account.model

import arrow.core.Either
import arrow.core.continuations.either.eager
import arrow.core.left
import arrow.core.right
import arrow.core.rightIfNotNull
import java.time.LocalDate

data class Pesel(
        val peselNumber: String
) {
    fun getDayOfBrith(): Either<AccountError.InvalidPeselFormat, LocalDate> = eager {
        val dayNumber = peselNumber.getIntByIndexes(4,6).bind()
        val monthNumber = peselNumber.getIntByIndexes(2,4).bind()
        val yearNumber = peselNumber.getIntByIndexes(0,2).bind()

        Either.catch {
            when (monthNumber) {
                in 1..12 -> LocalDate.of(yearNumber + 1900, monthNumber, dayNumber ).right()
                in 21..32 -> LocalDate.of(yearNumber + 2000, monthNumber - 20, dayNumber ).right()
                in 81..92 ->LocalDate.of(yearNumber + 1800, monthNumber - 80, dayNumber ).right()
                else -> AccountError.InvalidPeselFormat(peselNumber).left()
            }.bind()
        }.mapLeft { AccountError.InvalidPeselFormat(peselNumber) }.bind()
    }

    private fun String.getIntByIndexes(startIndex: Int, endIndex: Int) =
            this.substring(startIndex, endIndex)
                    .toIntOrNull()
                    .rightIfNotNull { AccountError.InvalidPeselFormat(peselNumber) }
}