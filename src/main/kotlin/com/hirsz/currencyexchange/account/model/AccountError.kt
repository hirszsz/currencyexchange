package com.hirsz.currencyexchange.account.model

import arrow.core.Nel
import com.hirsz.currencyexchange.ValidationConstraintViolation

sealed class AccountError {
    data class AccountWithPeselAlreadyRegistered(val pesel: String): AccountError()

    data class AccountWithPeselNotRegistered(val pesel: String): AccountError()

    data class AccountNotFound(val pesel: String): AccountError()

    data class InvalidPeselFormat(val pesel: String): AccountError()

    data class ValidationError(val violations: Nel<ValidationConstraintViolation>) : AccountError()

    data class RequiredAgeNotReached(val requiredAge: Int): AccountError()

    object NBPCommunicationError : AccountError()
}