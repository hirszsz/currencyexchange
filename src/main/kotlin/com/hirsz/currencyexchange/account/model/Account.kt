package com.hirsz.currencyexchange.account.model

data class Account(
        val firstName: String,
        val lastName: String,
        val pesel: Pesel
) {
    fun getPeselNumber() = pesel.peselNumber
}