package com.hirsz.currencyexchange.account.model

enum class AccountCurrency {
    PLN, USD
}