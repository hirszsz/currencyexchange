package com.hirsz.currencyexchange

import arrow.core.Either
import arrow.core.NonEmptyList
import javax.validation.ConstraintViolation
import javax.validation.Validation.buildDefaultValidatorFactory
import javax.validation.Validator

object CurrencyExchangeValidator {
    private val validator: Validator = buildDefaultValidatorFactory().validator

    fun <T> validate(data: T): Either<NonEmptyList<ValidationConstraintViolation>, Unit> {
        val errors = validator.validate(data)
            .map { it.asValidationError() }
        return NonEmptyList.fromList(errors)
            .toEither { }
            .swap()
    }
}

private fun <T> ConstraintViolation<T>.asValidationError() =
    ValidationConstraintViolation(propertyPath.toString(), message)

data class ValidationConstraintViolation(
    val field: String,
    val message: String
)