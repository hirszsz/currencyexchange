package com.hirsz.currencyexchange.services.currencyExchangeService

import arrow.core.Either
import arrow.core.right
import com.hirsz.currencyexchange.account.model.AccountCurrency
import com.hirsz.currencyexchange.account.model.AccountError
import com.hirsz.currencyexchange.services.currencyExchangeService.model.ExchangeRateRequest
import com.hirsz.currencyexchange.services.currencyExchangeService.model.ExchangeRateResponse
import java.math.BigDecimal

class HardcodedCurrencyExchangeRateService : CurrencyRateExchangeService {
    private val PLNToUSDExchangeRate = BigDecimal.valueOf(4.6)
    private val USDToPLNExchangeRate = BigDecimal.valueOf(0.2)

    override fun getExchangeRate(request: ExchangeRateRequest): Either<AccountError, ExchangeRateResponse> =
            ExchangeRateResponse(
                    currencyToBuy = request.currencyToBuy,
                    currencyForSale = request.currencyForSale,
                    exchangeRate = request.calculateExchangeRate()
            ).right()

    private fun ExchangeRateRequest.calculateExchangeRate(): BigDecimal =
            when (this.currencyForSale) {
                AccountCurrency.PLN -> when (this.currencyToBuy) {
                    AccountCurrency.PLN -> BigDecimal.ONE
                    AccountCurrency.USD -> PLNToUSDExchangeRate
                }
                AccountCurrency.USD -> when (this.currencyToBuy) {
                    AccountCurrency.PLN -> USDToPLNExchangeRate
                    AccountCurrency.USD -> BigDecimal.ONE
                }
            }
}