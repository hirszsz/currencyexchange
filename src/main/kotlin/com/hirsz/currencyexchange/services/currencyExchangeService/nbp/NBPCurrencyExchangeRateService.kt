package com.hirsz.currencyexchange.services.currencyExchangeService.nbp

import arrow.core.Either
import arrow.core.continuations.either.eager
import arrow.core.rightIfNotNull
import com.hirsz.currencyexchange.account.model.AccountCurrency
import com.hirsz.currencyexchange.account.model.AccountError
import com.hirsz.currencyexchange.configuration.NBPApiProperties
import com.hirsz.currencyexchange.services.currencyExchangeService.CurrencyRateExchangeService
import com.hirsz.currencyexchange.services.currencyExchangeService.model.ExchangeRateRequest
import com.hirsz.currencyexchange.services.currencyExchangeService.model.ExchangeRateResponse
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.math.BigDecimal
import java.math.RoundingMode

class NBPCurrencyExchangeRateService(private val properties: NBPApiProperties) : CurrencyRateExchangeService {
    private var httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    private var retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(properties.apiUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
    private val service: NBPService = retrofit.create()

    private fun fetchUSDRate() = service.getRate().execute().body()

    override fun getExchangeRate(request: ExchangeRateRequest): Either<AccountError, ExchangeRateResponse> = eager {
        ExchangeRateResponse(
                currencyToBuy = request.currencyToBuy,
                currencyForSale = request.currencyForSale,
                exchangeRate = when (request.currencyToBuy) {
                    AccountCurrency.PLN -> when (request.currencyForSale) {
                        AccountCurrency.PLN -> BigDecimal.ONE
                        AccountCurrency.USD -> fetchPLNTOUsdRate().bind()
                    }
                    AccountCurrency.USD -> when (request.currencyForSale) {
                        AccountCurrency.PLN -> fetchUSDToPLNRate().bind()
                        AccountCurrency.USD -> BigDecimal.ONE
                    }
                }
        )
    }

    private fun fetchUSDToPLNRate(): Either<AccountError, BigDecimal> =
            fetchUSDRate()?.rates?.getOrNull(0)?.bid
                    .rightIfNotNull { AccountError.NBPCommunicationError }
                    .map { BigDecimal.ONE.divide(it, properties.roundingScale, RoundingMode.CEILING ) }

    private fun fetchPLNTOUsdRate(): Either<AccountError.NBPCommunicationError, BigDecimal> =
            fetchUSDRate()?.rates?.getOrNull(0)?.bid
                    .rightIfNotNull { AccountError.NBPCommunicationError }

}