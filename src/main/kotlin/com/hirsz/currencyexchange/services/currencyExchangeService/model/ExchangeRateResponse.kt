package com.hirsz.currencyexchange.services.currencyExchangeService.model

import com.hirsz.currencyexchange.account.model.AccountCurrency
import java.math.BigDecimal

data class ExchangeRateResponse(
        val currencyToBuy: AccountCurrency,
        val currencyForSale: AccountCurrency,
        val exchangeRate: BigDecimal
)