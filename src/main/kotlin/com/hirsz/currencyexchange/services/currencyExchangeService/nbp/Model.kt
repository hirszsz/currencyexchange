package com.hirsz.currencyexchange.services.currencyExchangeService.nbp

import java.math.BigDecimal


data class RatesResponse(
        val code: String,
        val rates: List<Rate>
)

data class Rate(
        val bid: BigDecimal,
        val ask: BigDecimal
)