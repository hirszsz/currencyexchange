package com.hirsz.currencyexchange.services.currencyExchangeService

import arrow.core.Either
import com.hirsz.currencyexchange.account.model.AccountError
import com.hirsz.currencyexchange.services.currencyExchangeService.model.ExchangeRateRequest
import com.hirsz.currencyexchange.services.currencyExchangeService.model.ExchangeRateResponse

interface CurrencyRateExchangeService {

    fun getExchangeRate(request: ExchangeRateRequest): Either<AccountError, ExchangeRateResponse>
}