package com.hirsz.currencyexchange.services.currencyExchangeService.model

import com.hirsz.currencyexchange.account.model.AccountCurrency

data class ExchangeRateRequest(
        val currencyToBuy: AccountCurrency,
        val currencyForSale: AccountCurrency
)