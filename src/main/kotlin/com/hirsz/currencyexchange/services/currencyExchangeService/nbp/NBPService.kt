package com.hirsz.currencyexchange.services.currencyExchangeService.nbp

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface NBPService {

    @Headers("Content-Type:application/json")
    @GET("/api/exchangerates/rates/c/usd/")
    fun getRate(): Call<RatesResponse>
}
