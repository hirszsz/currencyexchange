package com.hirsz.currencyexchange.web

import com.hirsz.currencyexchange.account.AccountFacade
import com.hirsz.currencyexchange.web.model.AccountCreateBody
import com.hirsz.currencyexchange.web.model.AccountResponse
import org.springframework.http.HttpStatus
import io.swagger.v3.oas.annotations.Operation
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/account")
class AccountEndpoint(private val accountFacade: AccountFacade) {

    @GetMapping("/{pesel}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            summary = "Fetch account by PESEL")
    fun findDomain(
            @PathVariable pesel: String
    ): AccountResponse? {
        return accountFacade.find(pesel)
                .map { AccountResponse.from(it) }
                .orNull() //TODO error response
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            summary = "Create account",
    )
    fun createDomain(
            @RequestBody body: AccountCreateBody,
    ): AccountResponse? {
        return accountFacade.create(body.toRequest())
                .map { AccountResponse.from(it) }
                .orNull() //TODO error response

    }

}