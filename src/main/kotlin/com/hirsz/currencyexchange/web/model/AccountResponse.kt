package com.hirsz.currencyexchange.web.model

import com.hirsz.currencyexchange.account.model.Account

data class AccountResponse(
        val firstName: String,
        val lastName: String,
        val pesel: String,
) {
    companion object {
        fun from(account: Account) = AccountResponse(
                firstName = account.firstName,
                lastName = account.lastName,
                pesel = account.pesel.peselNumber
        )
    }
}