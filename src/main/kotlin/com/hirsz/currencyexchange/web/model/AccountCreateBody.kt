package com.hirsz.currencyexchange.web.model

import com.hirsz.currencyexchange.account.usecases.CreateAccountUseCase

data class AccountCreateBody(
        val firstName: String,
        val lastName: String,
        val pesel: String,
) {
    fun toRequest(): CreateAccountUseCase.CreateRequest = CreateAccountUseCase.CreateRequest(
            firstName = firstName,
            lastName = lastName,
            pesel = pesel
    )

}